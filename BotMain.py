import discord
import random
import asyncio
import requests
from requests import get, codes
from BotUtils import SqlHelper

TOKEN = 'TOKEN REMOVED FOR SECURITY REASONS'
REQUEST_LIMIT = 100

client = discord.Client()
sql = SqlHelper()

# More subreddits can be appended to the dictionary below.
subreddits = [{'url': 'https://www.reddit.com/r/dankmemes/.json?limit=%d'},
            {'url': 'https://www.reddit.com/r/MemeEconomy/.json?limit=%d'},
            {'url': 'https://www.reddit.com/r/dankchristianmemes/.json?limit=%d'},
            {'url': 'https://www.reddit.com/r/darkmemes/.json?limit=%d'}]

# So random links don't end up getting sent.
img_extensions = ('.jpg', '.jpeg', '.png')


def populate_db():
    """
    Scans all the Subreddits in the subreddits dictionary, and then appends
    """
    headers = {'User-Agent': 'Gimme Reddit Memes 1.0'}

    for subreddit in subreddits:
        r = get(subreddit['url'] % REQUEST_LIMIT, headers=headers)
        if r.status_code == codes['ok']:
            data = r.json()
            for i in range(2, REQUEST_LIMIT):  # This is probably the worst idea ever. IndexError guaranteed.
                try:
                    if len(str(data['data']['children'][i]['data']['url'])) > 0:
                        if str(data['data']['children'][i]['data']['url']).lower().endswith(img_extensions):
                            sql.add_meme(data['data']['children'][i]['data']['url'])
                except IndexError as e:
                    print(str(e), i)
    print("Database populated with new memes.")



def get_newest_post():
    """
    Checks if there's any posts in the db.
    If there aren't any, the subreddits dictionary will be scraped.

    If there are posts in the db, a random one that hasn't been picked will be picked.
    """
    if len(sql.get_unused()) == 0:
        populate_db()
    
    try:
        url = sql.get_random_unused()
        sql.use_meme(url)
        return url
    except BaseException:
        pass


@client.event
async def on_ready():
    print('Logged in as', client.user.name, '(', client.user.id, ')')


@client.event
async def on_message(message):
    try:
        content = str(message.content)
        channel = message.channel

        if content.startswith("I'm ") or \
            content.startswith("im ") or \
            content.startswith("Im "):
            message_data = content.split(' ')
            del message_data[0]
            reply_back = 'Hi '
            for m in message_data:
                reply_back += str(m) + ' '
            reply_back = reply_back.strip() + ", I'm dad."
            await client.send_message(channel, reply_back)
        elif content.lower() == 'meme' or \
            content.lower() == '😂':
            await client.send_message(channel, get_newest_post())
    except BaseException:
        pass


async def background_loop():
    await client.wait_until_ready()
    while not client.is_closed:
        channel = client.get_channel('CHANGED, USE YOUR OWN DAMN CHANNEL ID')
        try:
            await client.send_message(channel, get_newest_post())
        except BaseException:
            pass
        await asyncio.sleep(3 * 60)  # Set to 3 minutes.


if __name__ == '__main__':
    client.loop.create_task(background_loop())
    client.run(TOKEN)