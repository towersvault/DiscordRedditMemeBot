import sqlite3
from random import randint

class SqlHelper:
    class TableMeme:
        create = 'CREATE TABLE meme (' \
                'id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' \
                'url TEXT NOT NULL DEFAULT \'\', ' \
                'used INT NOT NULL DEFAULT \'0\');'
        insert = 'INSERT INTO meme (url) VALUES (\'%s\');'
        select_all = 'SELECT * FROM meme;'
        select_url = 'SELECT * FROM meme WHERE url = \'%s\';'
        select_unused = 'SELECT * FROM meme WHERE used = 0;'
        mark_used = 'UPDATE meme SET used = 1 WHERE url = \'%s\';'
    
    def __init__(self):
        self.db = sqlite3.connect('meme.db', check_same_thread=False)

        try:
            self.db.execute(self.TableMeme.create)
            self.db.commit()
        except BaseException as e:
            print(str(e))
        
        self.cursor = self.db.cursor()
    
    def add_meme(self, url):
        try:
            self.cursor.execute(self.TableMeme.select_url % str(url))
            results = self.cursor.fetchall()
            if len(results) == 0:
                self.cursor.execute(self.TableMeme.insert % str(url))
                self.db.commit()
                return True  # If meme is new
        except BaseException as e:
            print(str(e))
            self.db.rollback()
        return False  # If meme already exists
    
    def use_meme(self, url):
        try:
            self.cursor.execute(self.TableMeme.mark_used % str(url))
            self.db.commit()
        except BaseException as e:
            print(str(e))
            self.db.rollback()

    def get_unused(self):
        self.cursor.execute(self.TableMeme.select_unused)
        results = self.cursor.fetchall()
        data = []
        if len(results) > 0:
            for r in results:
                data.append({'url': r[1]})
        return data
    
    def get_next_unused(self):
        unused = self.get_unused()
        if len(unused) == 0:
            return ''
        return unused[0]['url']

    def get_random_unused(self):
        unused = self.get_unused()
        if len(unused) == 0:
            return ''
        random_int = randint(0, len(unused))
        return unused[random_int]['url']
